# Portfolio
This is a [Portfolio](anthonyleung.xyz) of my self.

## Specifications
- [x] Responsive
- [x] Mobile Friendly
- [ ] SEO Friendly
- [ ] PWA
- [ ] Light Mode

## Tech Stack
- [React](https://reactjs.org/)
- [Next.js](https://nextjs.org/)
- [Netlify](https://www.netlify.com/)
- [TypeScript](https://www.typescriptlang.org/)
- [Styled Components](https://styled-components.com/)

## Installation
1. Clone the repo
```sh
git clone
```

2. Install yarn packages
```sh
yarn install
```

3. Run the project
```sh
yarn dev
```

## License
Distributed under the MIT License. See `LICENSE` for more information.

## Contact
Anthony Leung 
[email](mailto:work@anthonyleung.xyz) 
[LinkedIn](https://www.linkedin.com/in/anthonyleung0601/)

## Credits
Special thanks. Website designed by Nicole Tang. She is a UX/UI designer. Contact her at [email](mailto:worksleu@gmail.com) or [LinkedIn](https://www.linkedin.com/in/nicootang/).