import { Grid } from '~/components/Grid'
import { Section } from '~/components/Section'
import { SkillCard } from '~/components/SkillCard'
import { skills } from '~/data/skill'

export const SkillSection = () => {
    return (
        <Section title="Skills" id="skills" className="main-section">
            <Grid
                $columnCount={{
                    mobile: 1,
                    tablet: 2,
                    desktop: 3,
                }}
                $gap={8}
            >
                {skills.map((skill, index) => {
                    return <SkillCard key={index} skill={skill} />
                })}
            </Grid>
        </Section>
    )
}
