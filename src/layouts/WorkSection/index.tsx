import { CareerCard } from '~/components/CareerCard'
import { CodeSnippets } from '~/components/CodeSnippets'
import { Section } from '~/components/Section'
import { career } from '~/data/career'
import { DateTime } from 'luxon'

export const WorkSection = () => {
    return (
        <Section title="Experience" id="experience" className="main-section">
            {career.map((career, index) => {
                const endYear = career.endDate ? DateTime.fromISO(career.endDate).toFormat('yyyy') : undefined

                return (
                    <CodeSnippets endYear={endYear} key={index}>
                        <CareerCard
                            icon={career.icon}
                            title={career.title}
                            subtitle={career.subtitle}
                            startDate={career.startDate}
                            endDate={career.endDate}
                            description={career.description}
                            highlight={career.highlight}
                            skills={career.skills}
                        />
                    </CodeSnippets>
                )
            })}
        </Section>
    )
}
