'use client'

import Link from 'next/link'
import { FlexRow } from '~/components/Flexbox'
import { Icon } from '~/components/Icon'
import Typography from '~/components/Typography'

export const DownloadResume = () => {
    return (
        <FlexRow
            $padding={{
                top: 10,
                bottom: 4,
            }}
            className="main-section"
        >
            <Link href="/resume.pdf" target="_blank" rel="noopener noreferrer">
                <FlexRow $gap={1}>
                    <Typography>{'// View Full Résumé'}</Typography>
                    <Icon name="arrow-right" />
                </FlexRow>
            </Link>
        </FlexRow>
    )
}
