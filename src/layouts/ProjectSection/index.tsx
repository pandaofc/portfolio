import { Section } from '~/components/Section'
import { projects } from '~/data/project'
import { ProjectCard } from '~/components/ProjectCard'
import { Grid } from '~/components/Grid'

export const ProjectSection = () => {
    return (
        <Section title="Projects" id="projects" className="main-section">
            <Grid
                $gap={8}
                $columnCount={{
                    desktop: 3,
                    tablet: 2,
                    mobile: 1,
                }}
            >
                {projects
                    .sort((a, b) => (a.order > b.order ? 1 : -1))
                    .map((project, index) => (
                        <ProjectCard project={project} key={index} />
                    ))}
            </Grid>
        </Section>
    )
}
