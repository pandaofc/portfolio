'use client'

import Div from '../../components/Div'
import Container from '../../components/Container'
import { FlexRow } from '../../components/Flexbox'
import { Logo } from '../../components/Logo'
import { Menu } from '~/components/Menu'
import { Icon } from '~/components/Icon'
import { Social } from '~/components/Social'

export const Header = () => {
    return (
        <Div $boxShadow $position='sticky' $top={0} $zIndex={100} $backgroundColor='background' $width='100%' id="header">
            <Container $padding={{ x: 4, y: 5 }}>
                <FlexRow $justifyContent="space-between" $alignItems="center">
                    {/* Left (Logo & menu) */}
                    <FlexRow $alignItems="center" $gap={14}>
                        <Logo />
                        <Menu />
                    </FlexRow>
                    {/* Right (soical logo) */}
                    <FlexRow $alignItems="center" $justifyContent="space-between">
                        <Social />
                    </FlexRow>
                </FlexRow>
            </Container>
        </Div>
    )
}
