import { FlexColumn, FlexRow } from '~/components/Flexbox'
import { ProfileImage, ReponsiveDescription, ReponsiveSection } from './styles'

import Typography from '~/components/Typography'
import { aboutMe } from '~/data/about'
import { MainContainer } from '~/components/Container'

export const AboutSection = () => {
    return (
        <FlexRow id="about" $width="100%" className="main-section" $backgroundColor="backgroundSecondary">
            <MainContainer>
                <FlexRow $width="100%" $alignItems="center">
                    <ProfileImage alt="me" src="/me.png" width={0} height={0} sizes="100vw" />
                    <ReponsiveSection
                        $padding={{
                            desktop: {
                                y: 10,
                                x: 13,
                            },
                            tablet: {
                                y: 10,
                                x: 8,
                            },
                            mobile: {
                                y: 4,
                                x: 4,
                            },
                        }}
                        $flex={1}
                        $gap={6}
                        $flexWrap="wrap"
                    >
                        <FlexColumn $gap={2} $padding={{ bottom: 5 }} $border={{ bottom: true }}>
                            <Typography variant="subtitle">Hello, I am</Typography>
                            <Typography variant="headline3">{aboutMe.name}</Typography>
                            <Typography variant="subtitle">{aboutMe.title}</Typography>
                        </FlexColumn>
                        <ReponsiveDescription>
                            <Typography variant="body" fontWeight="normal">
                                {aboutMe.description}
                            </Typography>
                        </ReponsiveDescription>
                    </ReponsiveSection>
                </FlexRow>
            </MainContainer>
        </FlexRow>
    )
}

export default AboutSection
