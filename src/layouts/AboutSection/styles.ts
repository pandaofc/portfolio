'use client'

import Image from 'next/image'
import styled from 'styled-components'
import { FlexRow } from '~/components/Flexbox'
import { spacingString } from '~/styles/theme'

export const ProfileImage = styled(Image)`
    width: 30%;
    min-width: 200px;
    max-width: 300px;
    height: auto;
    aspect-ratio: 1/1;
`

export const ReponsiveDescription = styled(FlexRow)`
    flex: 1;
    @media (max-width: ${({ theme }) => theme.breakpoints.tablet}) {
        flex: 0 0 100%;
        width: 100%;
    }
`

export const ReponsiveSection = styled(FlexRow)`
    @media (max-width: ${({ theme }) => theme.breakpoints.mobile}) {
        width: 100%;
        flex: auto;
        padding-bottom: ${({ theme }) => spacingString(8, theme)};
    }
`
