import { IconName } from '~/components/Icon'

export type Project = {
    name: string
    link?: string
    description?: string
    tech: string[]
    group: 'company' | 'side-project' | 'freelance'
    type: 'full stack' | 'frontend' | 'backend' | 'mobile'
    figma: boolean
    order: number
    icon?: IconName
    bgImage?: string
}

export const projects: Project[] = [
    {
        icon: 'ccp',
        name: 'Compassionate Children Program HK',
        link: 'https://compassionatechildrenhk.org/en',
        group: 'freelance',
        description:
            'CCP is a collaborative project involving local school teachers and various experiential learning partners in Hong Kong, I am responsible for the frontend and CMS system, Next.js + SCSS is used for the frontend, and Strapi is used for the backend',
        tech: ['Next.js', 'SCSS', 'Strapi'],
        figma: true,
        type: 'full stack',
        order: 3,
        bgImage: 'ccp.png',
    },
    {
        name: 'Two Hands',
        icon: 'two-hands',
        link: 'https://two-hands.netlify.app/en',
        group: 'freelance',
        description:
            'A New Way to Discover Trusted Luxury Shops, Next.js + MUI is used for the frontend, and NestJS + Prisma + Refine is used for the backend',
        tech: ['Next.js', 'MUI', 'Refine', 'NestJS', 'Prisma', 'PostgreSQL', 'Netlify', 'GraphQL'],
        figma: true,
        type: 'full stack',
        order: 5,
        bgImage: 'two-hands.png',
    },
    {
        name: 'Spaceship',
        icon: 'spaceship',
        link: 'https://spaceshipapp.com',
        group: 'company',
        tech: ['Next.js', 'NestJS', 'Laravel', 'Storybook'],
        figma: true,
        type: 'full stack',
        order: 6,
        bgImage: 'spaceship.png',
    },
    {
        name: 'Chatout Keyboard',
        icon: 'chatout',
        group: 'company',
        link: 'https://chatout.app',
        tech: ['SwiftUI', 'Serviceless'],
        figma: true,
        type: 'mobile',
        order: 8,
        bgImage: 'chatout.png',
    },
    {
        name: 'Spaceship iOS App',
        icon: 'spaceship',
        group: 'company',
        link: '',
        tech: ['SwiftUI'],
        figma: true,
        type: 'mobile',
        order: 7,
        bgImage: 'spaceship-ios.png',
    },
    {
        name: 'Somis.xyz',
        icon: 'somis',
        group: 'company',
        link: 'https://beta.somis.xyz',
        tech: ['Next.js', 'SUI', 'Move', 'NestJS', 'Web3', 'TailwindCSS'],
        figma: true,
        type: 'frontend',
        order: 1,
        bgImage: 'somis.png',
    },
    {
        name: 'Releap.xyz',
        icon: 'releap',
        group: 'company',
        link: 'https://app.releap.xyz',
        tech: ['Next.js', 'Styled Components', 'SUI', 'Move', 'NestJS', 'Web3', 'Firebase', 'Ethereum'],
        figma: true,
        type: 'frontend',
        order: 2,
        bgImage: 'releap.png',
    },
    {
        name: 'CTF App',
        icon: 'rn',
        description: 'The CTF Hybrid Mobile App using React Native, CTF is a Hong Kong based jewelry company',
        group: 'freelance',
        link: 'https://apps.apple.com/hk/app/周大福會員計劃/id1047463880',
        tech: ['React Native'],
        figma: false,
        type: 'mobile',
        order: 9,
        bgImage: 'ctf.png',
    },
    {
        name: 'BTC Trade Algorithms System',
        icon: 'bitcoin',
        group: 'side-project',
        link: 'https://gitlab.com/pandaofc/trading-system',
        description: 'A bitcoin trading system demo usinf NestJS',
        tech: ['NestJs', 'PostgreSQL', 'CloudSQL', 'Google Cloud', 'TypeORM', 'Binace API'],
        figma: false,
        type: 'backend',
        order: 3,
        bgImage: 'bts.png',
    },
    {
        name: 'K Dollar Program Bank Point System',
        group: 'freelance',
        description: 'A bank point transfer system for K Dollar reward program',
        tech: ['NestJs', 'MySQL'],
        figma: false,
        type: 'backend',
        order: 10,
        bgImage: 'nestjs.png',
        icon: 'kdollar',
    },
]
