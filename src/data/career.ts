import { IconName } from '~/components/Icon'

export type Career = {
    icon: IconName
    title: string
    subtitle?: string
    description: string
    highlight?: string[]
    location: string
    endDate?: string
    startDate?: string
    skills?: string[]
}

export const career: Career[] = [
    {
        icon: 'releap',
        title: 'Reep Labs',
        subtitle: 'Frontend Engineer / Contract Developer',
        startDate: '2022-12-01',
        endDate: '2023-10-01',
        location: 'Remote',
        description: `Releap is a Hong Kong-based startup focused on Web3-related products, including the NFT marketplace.
As a frontend developer, I collaborated with backend developers to design APIs and assist with smart contract development for the marketplace and social platform.`,
        highlight: [
            'Development of a web3 marketplace application and web3 social network application.',
            'Engaged and consulted with Backend Developer about system architecture decisions.',
            'Gathered and distilled application insights to support User Experience (UX) design improvements.',
            'Optimised Search Engine Optimisation (SEO) and UX by redeveloping and maintaining a marketplace web application using Next.js and SSR.',
            'Minimised application development costs having advised on deployment of AWS Amplify.',
            'Assistance in improving the smart contract (SUI/ETH) to achieve our business goals.',
        ],
        skills: ['React', 'Next.js', 'NestJs', 'TypeScript', 'TailwindCSS', 'Solidity', 'Move', 'AWS', 'Amplify'],
    },
    {
        icon: 'spaceship',
        title: 'Spaceship(HK) Limited',
        subtitle: 'Senior Software Engineer / Product Manager',
        startDate: '2019-05-01',
        endDate: '2022-09-30',
        location: 'Hong Kong',
        description: `Spaceship is a tech startup based in Hong Kong that specializes in improving global logistics efficiency for commerce worldwide. 
I was responsible for creating an e-commerce system for logistics. I focus on designing the system's infrastructure, including the database schema. Additionally, I oversee a team of developers to ensure the system adheres to our product roadmap.`,
        highlight: [
            'Championed a focus on stability and performance, including analysis and optimisation, code improvement and defect resolution to improve User Experience (UX).',
            'Consulted at all levels to identify solution requirements while promoting designs to minimise and mitigate technical debt during rapid early-stage development.',
            'Facilitated Scrum events, such as sprint planning/review, demos, sprint retrospective and product backlog refinement.',
            'Called upon expertise in technology platforms/stacks to build solution architecture aligned with Agile methodologies.',
            'Proactively reviewed and reported on software code to maintain product quality and suggest improvements.',
            'Played key role in the development of microservice architecture during a period of business scaling.',
            'Developed e-commerce/warehouse systems while overseeing API integration/docking.',
            'Engaged with Designers to create a waybill query iOS App using SwiftUI.',
            'Collaborated with a multidisciplinary team to drive system modularisation.',
        ],
        skills: [
            'Laravel',
            'NestJS',
            'React',
            'SwiftUI',
            'MySQL',
            'PostgreSQL',
            'Redis',
            'ArgoCD',
            'Kubernetes',
            'AWS',
            'serverless',
        ],
    },
    {
        icon: 'yoho',
        title: 'Yoho Hong Kong Limited',
        subtitle: 'Analyst Programmer',
        startDate: '2017-02-01',
        endDate: '2019-03-31',
        location: 'Hong Kong',
        description: `Yoho is an e-commerce platform in Hong Kong that caters to consumers.
As the first technical team member of Yoho, I developed and maintained a cutting-edge B2C e-commerce platform in Hong Kong, which included ERP, POS, and VIP systems.`,
        highlight: [
            'Reduced system load and increased server performance having identified and resolved N+1 performance issues in Object Relational Mapping (ORM); improved report query and introduced CDN (Cloudflare), underpinned by performance and availability monitoring tools.',
            'Identified and implemented solutions to optimise website speed, security and performance while driving footfall through high-impact Search Engine Optimisation (SEO) strategies.',
            'Proactively advised on website improvements to drive traffic, including contributions to digital marketing and promotional campaigns.',
            'Led requirements management and business case development alongside data analysis and reporting.',
            'Improved existing system architecture from legacy technology to MVC architecture.',
            'Enhanced UX having re-architected and refactored front-end frameworks.',
            'Project-managed system migration from a single server to GCP.',
        ],
        skills: ['PHP', 'MySQL', 'Redis', 'GCP', 'CloudSQL', 'JQeury'],
    },
    {
        icon: 'morus',
        title: 'Morus',
        subtitle: 'Programmer Analyst',
        startDate: '2015-10-01',
        endDate: '2016-10-31',
        location: 'Hong Kong',
        description:
            'Morus provides technology solutions to help clients build their own platforms. I developed and maintained the entire platform.',
        highlight: [
            'Using PHP Sylius as a base and MYSQL as database to set up an online-shop platform and backend system for a company.',
            'Coding an IOS program with the team by using AWS and SWIFT for trading kids’ books online platform with AWS DynamoDB as database in which the users can register and log in with Facebook and Google+.',
        ],
        skills: ['PHP', 'MySQL', 'DynamoDB', 'Swift', 'AWS', 'Sylius', 'Symfony'],
    },
    {
        icon: 'grade',
        title: 'Graduation',
        subtitle: 'Bachelor of Computer Science and Information Engineering',
        startDate: '2011-09-01',
        endDate: '2015-06-30',
        description: 'National Taiwan University of Science and Technology (NTUST)',
        location: 'Taipei, Taiwan',
    },
]
