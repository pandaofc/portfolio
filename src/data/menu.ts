export const menuItems = [
    {
        label: 'About',
        href: 'about',
    },
    {
        label: 'Skills',
        href: 'skills',
    },
    {
        label: 'Projects',
        href: 'projects',
    },
    {
        label: 'Experience',
        href: 'experience',
    },

    // {
    //     label: 'Contact',
    //     href: 'contact',
    // }
]
