export const aboutMe = {
    name: 'Anthony Leung',
    title: 'Full stack developer / Senior Software Engineer',
    description: `With my vast knowledge in IT research, development, and support, I am confident in my ability to efficiently design user-friendly websites for diverse clients. 
    
My current focus lies in web3 or any innovative products, as I find the latest technological advancements intriguing.`,
}
