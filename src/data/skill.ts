import { Colors } from '~/styles/theme'

export type Skill = {
    category: string
    parent: number
    skills: string[]
    logStyle: 'laravel' | 'code' | 'javascript' | 'bash' | 'logger' | 'move'
    color: Colors
}

export const skills: Skill[] = [
    {
        category: 'Backend',
        parent: 80,
        skills: [
            'Laravel',
            'NestJS',
            'Node.js',
            'Strapi',
            'Express',
            'Serverless',
            'GraphQL',
            'REST API',
            'Socket.io',
            'Spring Boot',
        ],
        logStyle: 'laravel',
        color: 'yellow',
    },
    {
        category: 'Database',
        parent: 75,
        skills: ['MySQL', 'PostgreSQL', 'Redis', 'DynamoDB', 'Firestore', 'MongoDB', 'Prisma'],
        logStyle: 'code',
        color: 'red',
    },
    {
        category: 'Frontend',
        parent: 70,
        skills: ['ReactJS', 'NextJS', 'Tailwind CSS', 'Styled Components', 'JQuery', 'Refine', 'WordPress'],
        logStyle: 'javascript',
        color: 'green',
    },
    {
        category: 'DevOps',
        parent: 60,
        skills: ['Docker', 'AWS', 'Amplify', 'Google Cloud', 'Netlify'],
        logStyle: 'bash',
        color: 'purple',
    },
    {
        category: 'Mobile',
        parent: 50,
        skills: ['React Native', 'SwiftUI'],
        logStyle: 'logger',
        color: 'blue',
    },
    {
        category: 'Blockchain',
        parent: 50,
        skills: ['Move', 'Sui', 'Solidity', 'Remix'],
        logStyle: 'move',
        color: 'cyan',
    },
]
