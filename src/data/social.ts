import { IconName } from "~/components/Icon";

export const socialItems: { name: IconName; link: string }[] = [
    {
        name: 'email',
        link: 'mailto:work@anthonyleung.xyz',
    },
    {
        name: 'gitlab',
        link: 'https://gitlab.com/pandaofc',
    },
    {
        name: 'linkedin',
        link: 'https://www.linkedin.com/in/anthonyleung0601/',
    },
]
