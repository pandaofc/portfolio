'use client'

import { createGlobalStyle } from 'styled-components'

const styled = { createGlobalStyle }

const GlobalStyle = styled.createGlobalStyle`
    * {
        box-sizing: border-box;
    }

    body {
        margin: 0;
        padding: 0;
        scroll-behavior: smooth;
    }

    a {
        text-decoration: none;
    }

    .main-section {
        // for the skicky header
        margin-top: -100px;
        padding-top: 100px;
    }
`

export default GlobalStyle
