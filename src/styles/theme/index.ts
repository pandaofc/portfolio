export * from './utils'
export * from './types'

export const DefaultTheme = {
    colors: {
        background: '#1E2127',
        backgroundSecondary: '#13171E',
        backgroundTertiary: '#272B33',
        black: '#25272C',
        red: '#C4666D',
        yellow: '#D7A351',
        blue: '#5D8CBD',
        green: '#91A36D',
        purple: '#8760AB',
        cyan: '#528F9C',
        border: '#3D4148',
        gray: '#596070',
        gray2: '#909195',
        white: '#ffffff',
    },
    fontSizes: {
        overline: '0.625rem',
        caption: '0.75rem',
        body: '0.875rem',
        subtitle: '1rem',
        headline: '1.25rem',
        headline2: '1.5rem',
        headline3: '2.125rem',
        headline4: '3.75rem',
    },
    spacing: 0.25, // 4px
    breakpoints: {
        mobile: '750px',
        tablet: '1024px',
        desktop: '1200px',
    },
}

export default DefaultTheme
