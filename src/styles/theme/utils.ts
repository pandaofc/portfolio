import {
    BaseSpacingType,
    PointSpacingType,
    PositionSpacingType,
    ResponsiveSpacingType,
    SpacingType,
    Theme,
} from './types'

const isPositionSpacing = (spacing: SpacingType): spacing is PositionSpacingType => {
    return (
        typeof spacing === 'object' &&
        spacing !== null &&
        ('top' in spacing || 'right' in spacing || 'bottom' in spacing || 'left' in spacing)
    )
}

const isPointSpacing = (spacing: SpacingType): spacing is PointSpacingType => {
    return typeof spacing === 'object' && spacing !== null && ('x' in spacing || 'y' in spacing)
}

const isBaseSpacing = (spacing: SpacingType): spacing is BaseSpacingType => {
    return typeof spacing === 'number' || isPositionSpacing(spacing) || isPointSpacing(spacing)
}

const isResponsiveSpacing = (spacing: SpacingType): spacing is ResponsiveSpacingType => {
    return (
        typeof spacing === 'object' &&
        spacing !== null &&
        ('mobile' in spacing || 'tablet' in spacing || 'desktop' in spacing)
    )
}

const _spacingToCSS = (spacing: BaseSpacingType, theme: Theme, property: 'margin' | 'padding') => {
    if (typeof spacing === 'number') {
        return `${property}: ${spacingString(spacing, theme)}`
    }

    if (isPositionSpacing(spacing)) {
        const { top, right, bottom, left } = spacing
        return `${property}: ${top ? spacingString(top, theme) : '0'} ${right ? spacingString(right, theme) : '0'} ${
            bottom ? spacingString(bottom, theme) : '0'
        } ${left ? spacingString(left, theme) : '0'}`
    }

    if (isPointSpacing(spacing)) {
        const { x, y } = spacing
        return `${property}: ${y ? spacingString(y, theme) : '0'} ${x ? spacingString(x, theme) : '0'}`
    }
}

export const spacingToCSS = (spacing: SpacingType, theme: Theme, property: 'margin' | 'padding') => {
    if (isBaseSpacing(spacing)) {
        return _spacingToCSS(spacing, theme, property)
    }

    if (isResponsiveSpacing(spacing)) {
        return `
            ${
                spacing.mobile
                    ? `@media (max-width: ${theme.breakpoints.mobile}) { ${_spacingToCSS(
                          spacing.mobile,
                          theme,
                          property,
                      )} }`
                    : ''
            }
            ${
                spacing.tablet
                    ? `@media (min-width: ${theme.breakpoints.mobile}) and (max-width: ${theme.breakpoints.tablet}) { ${_spacingToCSS(
                          spacing.tablet,
                          theme,
                          property,
                      )} }`
                    : ''
            }
            ${spacing.desktop ? `${_spacingToCSS(spacing.desktop, theme, property)}` : ''}
        `
    }

    return ''
}

export const spacingString = (number: number, theme: Theme) => {
    return `${number * theme.spacing}rem`
}

export const borderToCSS = (
    border:
        | boolean
        | {
              top?: boolean
              right?: boolean
              bottom?: boolean
              left?: boolean
          }
        | undefined,
    theme: Theme,
) => {
    if (border === undefined) {
        return ''
    }
    if (typeof border === 'boolean') {
        return border ? `border: 1px solid ${theme.colors.border};` : ''
    }

    const { top, right, bottom, left } = border
    return `
        border-top: ${top ? `1px solid ${theme.colors.border}` : 'none'};
        border-right: ${right ? `1px solid ${theme.colors.border}` : 'none'};
        border-bottom: ${bottom ? `1px solid ${theme.colors.border}` : 'none'};
        border-left: ${left ? `1px solid ${theme.colors.border}` : 'none'};
    `
}
