import DefaultTheme from '.'

export type PositionSpacingType = {
    top?: number
    right?: number
    bottom?: number
    left?: number
}

export type PointSpacingType = {
    x?: number
    y?: number
}

export type BaseSpacingType = number | PositionSpacingType | PointSpacingType

export type ResponsiveSpacingType = {
    mobile?: BaseSpacingType
    tablet?: BaseSpacingType
    desktop?: BaseSpacingType
}

export type SpacingType = BaseSpacingType | ResponsiveSpacingType

export type Theme = typeof DefaultTheme
export type FontSizes = keyof Theme['fontSizes']
export type Colors = keyof Theme['colors']
export type Breakpoints = keyof Theme['breakpoints']
