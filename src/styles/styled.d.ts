import 'styled-components'
import { Theme as CustomTheme } from './theme'
declare module 'styled-components' {
    // eslint-disable-next-line
    export interface DefaultTheme extends CustomTheme {}
}
