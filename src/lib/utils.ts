import { Colors } from "~/styles/theme"

export const remStringToNumber = (remString: string) => {
    return Number(remString.replace('rem', ''))
}

export const remToPx = (rem: number) => {
    return rem * 16
}

export const debounce = (func: Function, wait: number) => {
    let timeout: ReturnType<typeof setTimeout>
    return function executedFunction(...args: any[]) {
        const later = () => {
            console.log('later', func)
            clearTimeout(timeout)
            func(...args)
        }
        clearTimeout(timeout)
        timeout = setTimeout(later, wait)
    }
}

export const randomColor = (): Colors => {
    const colors: Colors[] = [
        'red',
        'yellow',
        'blue',
        'green',
        'purple',
        'cyan',
    ]
    return colors[Math.floor(Math.random() * colors.length)]
}

export const color = (index: number): Colors => {
    const colors: Colors[] = [
        'red',
        'yellow',
        'blue',
        'green',
        'purple',
        'cyan',
    ]
    return colors[index % colors.length]
}