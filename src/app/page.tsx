import { AboutSection } from '~/layouts/AboutSection'
import { SkillSection } from '~/layouts/SkillSection'
import { WorkSection } from '~/layouts/WorkSection'
import { ProjectSection } from '~/layouts/ProjectSection'
import { DownloadResume } from '~/layouts/DownloadResume'

import { MainContainer } from '~/components/Container'
import { LineNumberContainer } from '~/components/LineNumberContainer'

export default function Home() {
    return (
        <>
            <AboutSection />
            <MainContainer>
                <LineNumberContainer>
                    <SkillSection />
                    <ProjectSection />
                    <WorkSection />
                    <DownloadResume />
                </LineNumberContainer>
            </MainContainer>
        </>
    )
}
