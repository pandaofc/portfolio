import type { Metadata } from 'next'
import { Roboto_Mono } from 'next/font/google'
import { Main } from '~/components/Main'
import { Header } from '~/layouts/Header'
import GlobalStyle from '~/styles/globalStyle'
import Providers from './provider'

const roboto = Roboto_Mono({
    subsets: ['latin'],
})

const metadata: Metadata = {
    title: "Anthony Leung's Portfolio",
    description: 'A Portfolio using Next.js, TypeScript, and Styled Components',
}

export default function RootLayout({ children }: { children: React.ReactNode }) {
    return (
        <html lang="en">
            <body className={roboto.className}>
                <Providers>
                    <GlobalStyle />
                    <Header />
                    <Main>{children}</Main>
                </Providers>
            </body>
        </html>
    )
}

export { metadata }
