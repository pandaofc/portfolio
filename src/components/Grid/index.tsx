'use client'

import styled from 'styled-components'
import Div from '../Div'
import { spacingString } from '~/styles/theme'

export interface GridProps {
    $columnCount:
        | number
        | {
              desktop?: number
              tablet?: number
              mobile?: number
          }
    $gap?: number
    onClick?(): void
}

export const Grid = styled(Div)<GridProps>`
    display: grid;
    grid-template-columns: repeat(
        ${({ $columnCount }) => {
            if (typeof $columnCount === 'number') {
                return $columnCount
            } else {
                return $columnCount.desktop
            }
        }},
        ${({ $columnCount, $gap = 0, theme }) => {
            if (typeof $columnCount === 'number') {
                return `calc(100% / ${$columnCount} -(${spacingString($gap, theme)} * (${
                    ($columnCount ?? 0) - 1
                } / ${$columnCount} )))`
            } else {
                return `calc(100% / ${$columnCount.desktop} - (${spacingString($gap, theme)} * (${
                    ($columnCount.desktop ?? 0) - 1
                } / ${$columnCount.desktop} )))`
            }
        }}
    );
    gap: ${({ theme, $gap = 0 }) => spacingString($gap, theme)};
    ${({ onClick }) => onClick && 'cursor: pointer;'}

    @media (max-width: ${(props) => props.theme.breakpoints.tablet}) {
        grid-template-columns: repeat(
            ${({ $columnCount }) => {
                const count =
                    typeof $columnCount === 'number' ? $columnCount : $columnCount.tablet || $columnCount.desktop
                return count
            }},
            ${({ $columnCount, theme, $gap = 0 }) => {
                const count =
                    typeof $columnCount === 'number' ? $columnCount : $columnCount.tablet || $columnCount.desktop
                return `calc(100% / ${count} - (${spacingString($gap, theme)} * (${(count ?? 0) - 1} / ${count} )))`
            }}
        );
    }

    @media (max-width: ${(props) => props.theme.breakpoints.mobile}) {
        grid-template-columns: repeat(
            ${({ $columnCount }) => {
                const count =
                    typeof $columnCount === 'number'
                        ? $columnCount
                        : $columnCount.mobile || $columnCount.tablet || $columnCount.desktop
                return count
            }},
            ${({ $columnCount, $gap = 0, theme }) => {
                const count =
                    typeof $columnCount === 'number'
                        ? $columnCount
                        : $columnCount.mobile || $columnCount.tablet || $columnCount.desktop
                return `calc(100% / ${count} - (${spacingString($gap, theme)} * (${(count ?? 0) - 1} / ${count} )))`
            }}
        );
    }
`
