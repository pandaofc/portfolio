'use client'

import styled from 'styled-components'
import Div from '../Div'

export const Container = styled(Div)`
    width: 100%;
    max-width: 1200px;
    margin: 0 auto;
`

export const MainContainer = styled.div`
    margin: 0 auto;
    max-width: 1440px;
    width: 100%;
`

export default Container
