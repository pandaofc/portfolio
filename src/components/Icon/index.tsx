import { ReactSVG } from 'react-svg'
import { Colors } from '~/styles/theme'

export type IconName =
    | 'email'
    | 'gitlab'
    | 'linkedin'
    | 'grade'
    | 'yoho'
    | 'releap'
    | 'morus'
    | 'spaceship'
    | 'arrow-right'
    | 'somis'
    | 'bitcoin'
    | 'chatout'
    | 'ccp'
    | 'two-hands'
    | 'rn'
    | 'kdollar'

type IconProps = {
    name: IconName
    color?: Colors
}

export const loadSVG = (svg: string | { src: string; height: number; width: number }) =>
    typeof svg === 'string' ? svg : svg.src

export const Icon = ({ name }: IconProps) => {
    return <ReactSVG src={`/icons/${name}.svg`} width={20} height={20} />
}
