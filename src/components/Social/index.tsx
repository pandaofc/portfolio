import Link from 'next/link'
import { Icon } from '../Icon'
import Typography from '../Typography'
import { FlexRow } from '../Flexbox'
import { socialItems } from '~/data/social'

export const Social = () => {
    return (
        <FlexRow $gap={6} $alignItems="center" >
            {socialItems.map((item, index) => {
                return (
                    <Link href={item.link} key={index} target="_blank" rel="noopener noreferrer">
                        <Typography color="white" hoverColor="gray2">
                            <Icon name={item.name} />
                        </Typography>
                    </Link>
                )
            })}
        </FlexRow>
    )
}
