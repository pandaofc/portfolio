'use client'

import styled from 'styled-components'

export const Main = ({ children }: { children?: React.ReactNode }) => {
    return (
        <StyledMain>
            {children}
        </StyledMain>
    )
}

const StyledMain = styled.main`
    width: 100%;
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    background-color: ${(props) => props.theme.colors.background};
`