import Image from 'next/image'

export const Logo = () => {
    return <Image src="/logo.svg" width={40} height={40} alt="logo" priority />
}
