'use client'

import { useEffect, useState } from 'react'
import styled from 'styled-components'
import { Colors } from '~/styles/theme'

export interface ProgressBarProps {
    progress: number
    color: Colors
}

export const ProgressBar = ({ progress, color }: ProgressBarProps) => {
    const [width, setWidth] = useState(0)
    useEffect(() => {
        setWidth(progress)
    }, [progress])

    return (
        <StyledProgressBarContainer>
            <StyledProgressBar $width={width} $color={color} />
        </StyledProgressBarContainer>
    )
}

const StyledProgressBarContainer = styled.div`
    width: 100%;
    height: 0.25rem;
    background-color: ${({ theme }) => theme.colors.black};
`

const StyledProgressBar = styled.div<{
    $width: number
    $color: Colors
}>`
    width: 100%;
    height: 100%;
    background: ${({ theme, $color }) =>
        `linear-gradient(270deg, ${theme.colors[$color]} 0.08%, ${theme.colors.black} 100%)`};
    transition: width 0.5s ease-in-out;
    width: ${({ $width }) => $width}%;
`

export default ProgressBar
