'use client'

import styled from 'styled-components'
import { Project } from '~/data/project'
import { FlexColumn, FlexRow } from '../Flexbox'
import { Icon } from '../Icon'
import Typography from '../Typography'
import { SkillRow } from '../SkillRow'
import Link from 'next/link'
import Marquee from 'react-fast-marquee'
import { useEffect, useRef, useState } from 'react'
import { CSSTransition } from 'react-transition-group'
import Div from '../Div'
export const ProjectCard = ({ project }: { project: Project }) => {
    const [hover, setHover] = useState(false)
    const [inCenter, setInCenter] = useState(false)

    const ref = useRef<HTMLDivElement>(null)

    useEffect(
        () => {
            if (!ref.current) return
            const observer = new IntersectionObserver(
                ([entry]) => {
                    setInCenter(entry.isIntersecting)
                },
                {
                    root: null,
                    rootMargin: '-40% 0px -30% 0px',
                    threshold: 0.5,
                },
            )
            observer.observe(ref.current)
            return () => {
                observer.disconnect()
            }
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [],
    )

    return (
        <Link
            href={project.link ? project.link : ''}
            target="_blank"
            rel="noopener noreferrer"
            onClick={(e) => {
                if (project.link === undefined || project.link === '') {
                    e.preventDefault()
                }
            }}
        >
            <StyledProjectCard
                ref={ref}
                $padding={6}
                $border
                onMouseEnter={() => setHover(true)}
                onMouseLeave={() => setHover(false)}
                $bgImage={project.bgImage ? `/projects/${project.bgImage}` : undefined}
                $clickable={project.link !== undefined && project.link !== ''}
                $inCenter={inCenter}
            >
                <FlexColumn $gap={4}>
                    <FlexColumn $gap={2}>
                        {project.icon && <Icon name={project.icon} />}
                        <Typography variant="body" tranform="capitalize">
                            {project.group} | {project.type}
                        </Typography>
                        <Typography variant="headline2">{project.name}</Typography>
                    </FlexColumn>
                    {/* <Typography variant="body" color="gray" style={{ opacity: 0 }}>
                            {project.description}
                        </Typography> */}
                    {!hover && (
                        <Div>
                            {project.tech.length > 99 ? (
                                <Marquee gradient={false} speed={40} loop={0} pauseOnHover={true}>
                                    <SkillRow skills={project.tech} />
                                    &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;
                                </Marquee>
                            ) : (
                                <SkillRow skills={project.tech} variant="caption" />
                            )}
                        </Div>
                    )}
                </FlexColumn>
            </StyledProjectCard>
        </Link>
    )
}

const StyledProjectCard = styled(FlexColumn)<{
    $bgImage?: string
    $clickable?: boolean
    $inCenter?: boolean
}>`
    justify-content: flex-end;
    user-select: none;
    cursor: ${({ $clickable }) => ($clickable ? 'pointer' : 'default')};
    background: ${({ theme }) => theme.colors.backgroundTertiary};
    height: 300px;
    transition: background 200ms ease-in-out;
    &::before {
        transition: opacity 200ms ease-in-out, background 200ms ease-in-out;
        content: '';
        background-image: url(${({ $bgImage }) => $bgImage});
        background-size: cover;
        position: absolute;
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
        opacity: ${({ $inCenter }) => ($inCenter ? 0.1 : 0)};
    }

    &:hover {
        background: linear-gradient(0deg, #272b33 -2.02%, rgba(39, 43, 51, 0) 70.77%)
            ${({ $bgImage }) => ($bgImage ? `, url(${$bgImage})` : '')};
        background-size: cover;
        background-position: center;
        &::before {
            background-size: cover;
            background-position: center;
        }
    }

    @media (max-width: ${({ theme }) => theme.breakpoints.tablet}) {
        background: none;
        background-size: cover;
        background-position: center;
        &::before {
            background: linear-gradient(0deg, #272b33 -2.02%, rgba(39, 43, 51, 0) 70.77%)
                ${({ $bgImage }) => ($bgImage ? `, url(${$bgImage})` : '')};
            background-size: cover;
            background-position: center;
            opacity: 0.3;
        }
        &:hover {
            background: none;
            &::before {
                opacity: 0.8;
            }
        }
    }
`
