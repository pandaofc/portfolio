import { Skill } from '~/data/skill'
import { FlexColumn, FlexRow } from '../Flexbox'
import ProgressBar from '../ProgressBar'
import Typography from '../Typography'
import { LogDisplay } from '../LogDisplay'

export const SkillCard = ({ skill }: { skill: Skill }) => {
    const { category, parent, skills, logStyle, color } = skill
    return (
        <FlexColumn $gap={4} $padding={{
            bottom: 8,
        }}>
            <ProgressBar progress={parent} color={color} />
            <FlexRow $gap={2} $alignItems="center">
                <Typography variant="headline3">{category}</Typography>
                {/* <Typography variant="headline2" color={color}>{`${parent}%`}</Typography> */}
            </FlexRow>
            <LogDisplay logs={skills} logStyle={logStyle} />
        </FlexColumn>
    )
}
