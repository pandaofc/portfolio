import { useCallback } from 'react'
import { FlexColumn, FlexRow } from '../Flexbox'
import Typography from '../Typography'
import { FontSizes } from '~/styles/theme'

type LogDisplayProps = {
    logs: string[]
    logStyle: 'laravel' | 'code' | 'javascript' | 'bash' | 'logger' | 'move'
    perLine?: number
}

const baseProps = {
    variant: 'caption',
    fontWeight: 'bold',
} as {
    variant: FontSizes
    fontWeight: 'normal' | 'bold'
}

export const LogDisplay = ({ logs, logStyle, perLine = 3 }: LogDisplayProps) => {
    const splitLogs = logs.reduce((acc, log, index) => {
        const line = Math.floor(index / perLine)
        if (!acc[line]) {
            acc[line] = []
        }
        acc[line].push(log)
        return acc
    }, [] as string[][]) as string[][]

    const renderLog = useCallback(
        (logs: string[]) => {
            switch (logStyle) {
                // Log::info('message');
                case 'laravel':
                    return (
                        <>
                            <Typography color="yellow" {...baseProps}>
                                Log
                            </Typography>
                            <Typography color="white" {...baseProps}>
                                ::
                            </Typography>
                            <Typography color="blue" {...baseProps}>
                                info
                            </Typography>
                            <Typography color="white" {...baseProps}>
                                (
                            </Typography>
                            <FlexRow>
                                {logs.map((log, i) => {
                                    return (
                                        <FlexRow key={i}>
                                            <Typography color="green" {...baseProps}>
                                                &apos;{log}&apos;
                                            </Typography>
                                            {i !== logs.length - 1 && (
                                                <Typography color="white" {...baseProps}>
                                                    .
                                                </Typography>
                                            )}
                                        </FlexRow>
                                    )
                                })}
                            </FlexRow>
                            <Typography {...baseProps} color="white">
                                );
                            </Typography>
                        </>
                    )
                // console.log('message');
                case 'javascript':
                    return (
                        <>
                            <Typography color="yellow" {...baseProps}>
                                console
                            </Typography>
                            <Typography color="white" {...baseProps}>
                                .
                            </Typography>
                            <Typography color="red" {...baseProps}>
                                log
                            </Typography>
                            <Typography color="white" {...baseProps}>
                                (
                            </Typography>
                            {
                                <FlexRow $gap={1}>
                                    {logs.map((log, i) => {
                                        return (
                                            <FlexRow key={i}>
                                                <Typography color="green" {...baseProps}>
                                                    &apos;{log}&apos;
                                                </Typography>
                                                {i !== logs.length - 1 && (
                                                    <Typography color="white" {...baseProps}>
                                                        ,
                                                    </Typography>
                                                )}
                                            </FlexRow>
                                        )
                                    })}
                                </FlexRow>
                            }
                            <Typography color="white" {...baseProps}>
                                )
                            </Typography>
                        </>
                    )
                // echo 'message';
                case 'bash':
                    return (
                        <FlexRow $gap={1}>
                            <Typography color="blue" {...baseProps}>
                                echo
                            </Typography>
                            <Typography color="white" {...baseProps}>
                                &quot;{logs.join(' ')}&quot;
                            </Typography>
                        </FlexRow>
                    )
                // logger.log('message');
                case 'logger':
                    return (
                        <FlexRow>
                            <Typography color="red" {...baseProps}>
                                logger
                            </Typography>
                            <Typography color="white" {...baseProps}>
                                .
                            </Typography>
                            <Typography color="blue" {...baseProps}>
                                log
                            </Typography>
                            <Typography color="red" {...baseProps}>
                                (
                            </Typography>
                            <Typography color="green" {...baseProps}>
                                &quot;{logs.join(' ')}&quot;
                            </Typography>
                            <Typography color="red" {...baseProps}>
                                )
                            </Typography>
                            <Typography color="white" {...baseProps}>
                                ;
                            </Typography>
                        </FlexRow>
                    )
                //  std::debug::print
                case 'move':
                    return (
                        <FlexRow>
                            <Typography color="yellow" {...baseProps}>
                                std
                            </Typography>
                            <Typography color="white" {...baseProps}>
                                ::
                            </Typography>
                            <Typography color="yellow" {...baseProps}>
                                debug
                            </Typography>
                            <Typography color="white" {...baseProps}>
                                ::
                            </Typography>
                            <Typography color="blue" {...baseProps}>
                                print
                            </Typography>
                            <Typography color="white" {...baseProps}>
                                (
                            </Typography>
                            <Typography color="green" {...baseProps}>
                                &quot;{logs.join(' ')}&quot;
                            </Typography>
                            <Typography color="white" {...baseProps}>
                                );
                            </Typography>
                        </FlexRow>
                    )
                // <> message </>
                case 'code':
                default:
                    return (
                        <>
                            <Typography color="white" {...baseProps}>
                                {'<> '}
                            </Typography>
                            <Typography color="cyan" {...baseProps}>
                                {logs.join(' ')}
                            </Typography>
                            <Typography color="white" {...baseProps}>
                                {' </>'}
                            </Typography>
                        </>
                    )
            }
        },
        [logStyle],
    )

    return (
        <FlexColumn $gap={1} $width="100%">
            {splitLogs.map((logs, index) => (
                <FlexRow $backgroundColor="border" $padding={1} key={index}>
                    {renderLog(logs)}
                </FlexRow>
            ))}
        </FlexColumn>
    )
}
