'use client'

import styled from 'styled-components'
import { SpacingType, spacingToCSS, FontSizes, Colors, Breakpoints, borderToCSS } from '~/styles/theme'

export type DivProps = {
    $padding?: SpacingType
    $margin?: SpacingType
    $fontSize?: FontSizes
    $fontWeight?: 'normal' | 'bold'
    $color?: Colors
    $backgroundColor?: Colors
    $border?:
        | boolean
        | {
              top?: boolean
              bottom?: boolean
              left?: boolean
              right?: boolean
          }
    $boxShadow?: boolean
    $children?: React.ReactNode
    $width?: string
    $height?: string
    $flexShrink?: number
    $position?: 'absolute' | 'relative' | 'fixed' | 'sticky'
    $top?: number
    $left?: number
    $right?: number
    $bottom?: number
    $zIndex?: number
    $hiddenAs?: Breakpoints
    $opacity?: number
    $minHeight?: string
}

export const Div = styled.div<DivProps>`
    position: ${({ $position }) => ($position ? $position : 'relative')};
    ${({ $top }) => (typeof $top === 'number' ? `top: ${$top}px;` : '')}
    ${({ $left }) => (typeof $left === 'number' ? `left: ${$left}px;` : '')}
    ${({ $right }) => (typeof $right === 'number' ? `right: ${$right}px;` : '')}
    ${({ $bottom }) => (typeof $bottom === 'number' ? `bottom: ${$bottom}px;` : '')}
    ${({ $zIndex }) => (typeof $zIndex === 'number' ? `z-index: ${$zIndex};` : '')}
    ${({ $padding, theme }) => ($padding ? spacingToCSS($padding, theme, 'padding') : '')};
    ${({ $margin, theme }) => ($margin ? spacingToCSS($margin, theme, 'margin') : '')};
    font-size: ${({ $fontSize, theme }) => ($fontSize ? $fontSize : theme.fontSizes.body)};
    font-weight: ${({ $fontWeight }) => ($fontWeight ? $fontWeight : 'normal')};
    color: ${({ color, theme }) => (color ? color : theme.colors.white)};
    background-color: ${({ $backgroundColor, theme }) =>
        !$backgroundColor ? 'transparent' : theme.colors[$backgroundColor]};
    ${({ $border, theme }) => borderToCSS($border, theme)};
    box-shadow: ${({ $boxShadow }) => ($boxShadow ? '0px 4px 12px 0px rgba(0, 0, 0, 0.45);' : 'none')};
    width: ${({ $width }) => ($width ? $width : 'auto')};
    height: ${({ $height }) => ($height ? $height : 'auto')};
    flex-shrink: ${({ $flexShrink }) => ($flexShrink ? $flexShrink : 0)};
    ${({ $hiddenAs, theme }) =>
        $hiddenAs ? `@media (max-width: ${theme.breakpoints[$hiddenAs]}) { display: none!important; }` : ''}
    ${({ $opacity }) => ($opacity ? `opacity: ${$opacity};` : '')}
    ${({ $minHeight }) => ($minHeight ? `min-height: ${$minHeight};` : '')}
`

export default Div
