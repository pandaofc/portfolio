'use client'

import styled from 'styled-components'
import { Colors, FontSizes, Theme } from '~/styles/theme'

const typeSystem = (variant: FontSizes, theme: Theme) => {
    switch (variant) {
        case 'headline4':
            return `
                font-size: ${theme.fontSizes.headline4};
                font-style: normal;
                font-weight: 300;
                line-height: normal;
            `
        case 'headline3':
            return `
                font-size: ${theme.fontSizes.headline3};
                font-style: normal;
                font-weight: 300;
                line-height: normal;
            `
        case 'headline2':
            return `
                font-size: ${theme.fontSizes.headline2};
                font-style: normal;
                font-weight: 700;
                line-height: 150%;
            `
        case 'headline':
            return `
                font-size: ${theme.fontSizes.headline};
                font-style: normal;
                font-weight: 400;
                line-height: 200%;
            `
        case 'subtitle':
            return `
                font-size: ${theme.fontSizes.subtitle};
                font-style: normal;
                font-weight: 400;
                line-height: 150%;
            `
        case 'body':
            return `
                font-size: ${theme.fontSizes.body};
                font-style: normal;
                font-weight: 700;
                line-height: normal;
                letter-spacing: 0.07rem;
            `
        case 'caption':
            return `
                font-size: ${theme.fontSizes.caption};
                font-style: normal;
                font-weight: 400;
                line-height: normal;
            `
        case 'overline':
            return `
                font-size: ${theme.fontSizes.overline};
                font-style: normal;
                font-weight: 400;
                line-height: normal;
            `
    }
}

export type TypographyProps = {
    $variant?: FontSizes
    $color?: Colors
    $hoverColor?: Colors
    $align?: 'left' | 'center' | 'right'
    $fontWeight?: 'normal' | 'bold'
    $tranform?: 'none' | 'uppercase' | 'lowercase' | 'capitalize'
} & React.HTMLAttributes<HTMLParagraphElement>

export const Typography = ({
    variant = 'body',
    color = 'white',
    align = 'left',
    tranform = 'none',
    fontWeight,
    hoverColor = undefined,
    component = 'span',
    children,
    ...rest
}: {
    variant?: FontSizes
    color?: Colors
    hoverColor?: Colors
    tranform?: 'none' | 'uppercase' | 'lowercase' | 'capitalize'
    align?: 'left' | 'center' | 'right'
    component?: 'p' | 'span' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'label' | 'div' | 'a'
    fontWeight?: 'normal' | 'bold'
    children?: React.ReactNode
    style?: React.CSSProperties
}) => {
    return (
        <StyledSpan
            as={component}
            $variant={variant}
            $color={color}
            $hoverColor={hoverColor}
            $align={align}
            $fontWeight={fontWeight}
            $tranform={tranform}
            {...rest}
        >
            {children}
        </StyledSpan>
    )
}

export default Typography

const StyledSpan = styled.span<TypographyProps>`
    color: ${({ $color, theme }) => ($color ? theme.colors[$color] : theme.colors.white)};
    ${({ $variant, theme }) => ($variant ? typeSystem($variant, theme) : typeSystem('body', theme))};
    ${({ $hoverColor, theme }) => $hoverColor && `&:hover { color: ${theme.colors[$hoverColor]}; }`};
    ${({ $align }) => $align && `text-align: ${$align};`};
    white-space: pre-wrap;
    word-wrap: break-word;
    ${({ $fontWeight }) => $fontWeight && `font-weight: ${$fontWeight};`};
    ${({ $tranform }) => $tranform && `text-transform: ${$tranform};`};
`
