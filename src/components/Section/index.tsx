import Typography from '../Typography'
import { StyledSection } from './style'

type SectionProps = {
    title: string
    children?: React.ReactNode
} & React.HTMLAttributes<HTMLDivElement>

export const Section = ({ title, children, ...props }: SectionProps) => {
    return (
        <StyledSection {...props}>
            <Typography variant="subtitle" color="gray">
                {`// ${title}`}
            </Typography>
            {children}
        </StyledSection>
    )
}
