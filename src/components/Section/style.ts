'use client'

import styled from 'styled-components'
import { spacingString } from '~/styles/theme'

export const StyledSection = styled.section`
    display: flex;
    flex-direction: column;
    gap: ${({ theme }) => spacingString(4, theme)};
    padding-bottom: ${({ theme }) => spacingString(20, theme)};
`
