'use client'

import styled from 'styled-components'
import Div, { DivProps } from '../Div'
import { spacingString } from '~/styles/theme'

export type FlexboxProps = {
    $flexDirection?: 'row' | 'column'
    $justifyContent?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around'
    $alignItems?: 'flex-start' | 'flex-end' | 'center' | 'stretch'
    $flexWrap?: 'wrap' | 'nowrap'
    $gap?: number
    $flex?: number
} & DivProps

export const Flexbox = styled(Div)<FlexboxProps>`
    display: flex;
    flex-direction: ${({ $flexDirection }) => $flexDirection ?? 'row'};
    justify-content: ${({ $justifyContent }) => $justifyContent ?? 'flex-start'};
    align-items: ${({ $alignItems }) => $alignItems ?? 'flex-start'};
    flex-wrap: ${({ $flexWrap }) => $flexWrap ?? 'wrap'};
    flex: ${({ $flex }) => $flex ?? 'initial'};
    ${({ $gap, theme }) => ($gap ? `gap: ${spacingString($gap, theme)}` : '')};
`

export const FlexColumn = styled(Flexbox)`
    flex-direction: column;
`

export const FlexRow = styled(Flexbox)`
    flex-direction: row;
`

export default Flexbox
