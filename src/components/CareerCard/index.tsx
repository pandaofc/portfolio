'use client'

import { ReactNode, useCallback, useRef, useState } from 'react'
import { Icon, IconName } from '../Icon'
import styled from 'styled-components'
import { FlexColumn, FlexRow } from '../Flexbox'
import { spacingString } from '~/styles/theme'
import Typography from '../Typography'
import { DateTime } from 'luxon'
import { color } from '~/lib/utils'
import { SkillRow } from '../SkillRow'

type CareerCardProps = {
    icon: IconName
    title: string
    subtitle?: string
    startDate?: string
    endDate?: string
    description?: string | string[]
    highlight?: string[]
    skills?: string[]
}

export const CareerCard = ({
    icon,
    title,
    subtitle,
    startDate,
    endDate,
    description,
    skills,
    highlight,
}: CareerCardProps) => {
    const [readMore, setReadMore] = useState(false)

    const ref = useRef<HTMLUListElement>(null)
    const totalDiff =
        endDate && startDate
            ? DateTime.fromISO(endDate).diff(DateTime.fromISO(startDate), ['years', 'months', 'days'])
            : undefined
    const startYear = startDate ? DateTime.fromISO(startDate).toFormat('yyyy') : undefined
    const total = useCallback(() => {
        if (!totalDiff) return null
        const { years, months, days } = totalDiff.toObject()
        if (years) return `${years} Year${years > 1 ? 's' : ''}`
        if (months) return `${months} Month${months > 1 ? 's' : ''}`
        if (days) return `${days} Day${days > 1 ? 's' : ''}`
        return null
    }, [totalDiff])
    return (
        <StyledCareerCard
            $padding={6}
            $gap={4}
            onClick={() => {
                setReadMore((prev) => !prev)
            }}
        >
            <Icon name={icon} />
            <FlexColumn $gap={2}>
                <FlexColumn $gap={1}>
                    <Typography variant="headline3" color="white">
                        {title}
                    </Typography>
                    {subtitle && (
                        <Typography variant="caption" color="white">
                            {subtitle}
                        </Typography>
                    )}
                </FlexColumn>
                <Typography variant="subtitle" color="gray" fontWeight="bold">
                    {total() && `${total()} / `}
                    {startYear}
                </Typography>
            </FlexColumn>
            <FlexColumn $gap={3} $padding={{ bottom: 3 }}>
                {description ? (
                    <Typography variant="body" color="white">
                        {description}
                    </Typography>
                ) : (
                    <></>
                )}

                <StyledUl ref={ref} $maxHeight={readMore ? 600 : 0}>
                    {(highlight ?? []).map((h, index) => (
                        <li key={index}>
                            <Typography variant="body" color="gray2" fontWeight="normal">
                                {h}
                            </Typography>
                        </li>
                    ))}
                </StyledUl>
            </FlexColumn>
            <SkillRow skills={skills} />
        </StyledCareerCard>
    )
}

const StyledCareerCard = styled(FlexColumn)`
    user-select: none;
    cursor: pointer;
    background-color: ${({ theme }) => theme.colors.backgroundSecondary};
    max-width: 50%;
    @media (max-width: ${({ theme }) => theme.breakpoints.mobile}) {
        max-width: 100%;
    }
    &:hover {
        background-color: ${({ theme }) => theme.colors.border};
    }
`

const StyledUl = styled.ul<{
    $maxHeight?: number
}>`
    max-height: ${({ $maxHeight }) => $maxHeight ?? 0}px;
    transition: ${({ $maxHeight }) => ($maxHeight ? 'max-height 0.15s ease-out' : 'max-height 0.25s ease-in')};
    padding-left: ${({ theme }) => spacingString(4, theme)};
    color: ${({ theme }) => theme.colors.gray2};
    margin: 0;
    overflow: hidden;
    & > li:not(:last-child) {
        padding-bottom: ${({ theme }) => spacingString(4, theme)};
    }
`
