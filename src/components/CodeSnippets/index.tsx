import Div from '../Div'
import { FlexColumn } from '../Flexbox'
import Typography from '../Typography'

export const CodeSnippets = ({
    startYear,
    endYear,
    children,
}: {
    startYear?: string
    endYear?: string
    children?: React.ReactNode
}) => {
    return (
        <Div $width="100%" $minHeight="100px" $position="relative">
            <FlexColumn $height="100%" $position="absolute">
                <FlexColumn
                    $padding={{
                        left: 1,
                    }}
                >
                    {endYear && <Typography>{endYear}:</Typography>}
                    <Typography color="yellow">{'{'}</Typography>
                </FlexColumn>
                <FlexColumn
                    $border={{
                        left: true,
                    }}
                    $flex={1}
                />
                <FlexColumn
                    $padding={{
                        left: 1,
                    }}
                >
                    <Typography color="yellow">{'}'}</Typography>
                    {startYear && <Typography>{startYear}</Typography>}
                </FlexColumn>
            </FlexColumn>
            <Div
                $padding={{
                    desktop: {
                        left: 24,
                    },
                    tablet: {
                        left: 16,
                    },
                    mobile: {
                        left: 3,
                        top: 12,
                        bottom: 8,
                    },
                }}
            >
                {children}
            </Div>
        </Div>
    )
}
