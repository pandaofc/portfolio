'use client'

import styled from 'styled-components'
import { FlexRow } from '../Flexbox'
import Typography from '../Typography'
import { color } from '~/lib/utils'
import { FontSizes } from '~/styles/theme'

export const SkillRow = ({ skills, variant = 'body' }: { skills?: string[]; variant?: FontSizes }) => {
    return (
        <StyledSkillRow $showPrefix={!!skills}>
            {skills?.map((skill, index) => (
                <Typography color={color(index)} key={index} variant={variant}>
                    {`${skill}`}
                </Typography>
            ))}
        </StyledSkillRow>
    )
}

const StyledSkillRow = styled(FlexRow)<{
    $showPrefix: boolean
}>`
    position: relative;
    min-height: 20px;
    gap: 0.75rem;
    ${({ $showPrefix, theme }) => $showPrefix && `padding-left: 40px;`}

    ${({ $showPrefix, theme }) =>
        $showPrefix &&
        `&::before { 
        content: '</>';
        position: absolute;
        left: 0;
        color: ${theme.colors.gray};
        }`}
`
