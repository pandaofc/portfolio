import Link from 'next/link'
import { FlexRow } from '../Flexbox'
import Typography from '../Typography'
import { menuItems } from '~/data/menu'
import { useEffect, useState } from 'react'
import styled from 'styled-components'

export const Menu = () => {
    const [activeIndex, setActiveIndex] = useState(0)

    useEffect(() => {
        const handleScroll = () => {
            const headerHeight = document.getElementById('header')?.getBoundingClientRect().height || 0
            const menuItemsPositions = menuItems
                .map((item) => {
                    const el = document.getElementById(item.href)
                    if (el) {
                        const bounding = el.getBoundingClientRect()
                        console.log({ bounding })
                        return {
                            top: el.offsetTop > 0 ? el.offsetTop : 0,
                            height: bounding.height - headerHeight,
                        }
                    }
                    return {
                        top: 0,
                        height: 0,
                    }
                })
                .reduce((acc, cur, index) => {
                    if (index === 0) {
                        return [
                            {
                                start: 0,
                                end: cur.height,
                            },
                        ]
                    }
                    return [
                        ...acc,
                        {
                            start: acc[index - 1].end - 10,
                            end: cur.height + acc[index - 1].end,
                        },
                    ]
                }, [] as { start: number; end: number }[])
            const scrollPosition = window.scrollY

            const activeIndex = menuItemsPositions.reduce((acc, cur, index) => {
                if (scrollPosition + headerHeight >= cur.start && scrollPosition + headerHeight <= cur.end) {
                    return index
                }
                return acc
            }, -1)
            if (activeIndex === -1) {
                setActiveIndex(0)
            } else {
                setActiveIndex(activeIndex)
            }
        }

        window.addEventListener('scroll', handleScroll)

        return () => {
            window.removeEventListener('scroll', handleScroll)
        }
    }, [])

    return (
        <FlexRow $gap={14} $alignItems="center" $hiddenAs="mobile">
            {menuItems.map((item, index) => (
                <StyledLink
                    href={`/#${item.href}`}
                    key={item.label}
                    $active={activeIndex === index}
                    scroll={false}
                    onClick={(e) => {
                        e.preventDefault()
                        document.getElementById(item.href)?.scrollIntoView({ behavior: 'smooth' })
                    }}
                >
                    <Typography color={activeIndex === index ? 'white' : 'gray'} variant="subtitle" hoverColor="white">
                        {item.label}
                    </Typography>
                    <StyledBottomLine $active={activeIndex === index} />
                </StyledLink>
            ))}
        </FlexRow>
    )
}

export default Menu

const StyledLink = styled(Link)<{ $active: boolean }>``

const StyledBottomLine = styled.div<{ $active: boolean }>`
    width: 100%;
    height: 2px;
    background-color: ${({ theme }) => theme.colors.yellow};
    opacity: ${({ $active }) => ($active ? 1 : 0)};
    transition: opacity 0.2s ease-in-out;
`
