'use client'

import styled, { useTheme } from 'styled-components'
import Container from '../Container'
import Div from '../Div'
import { useRefDimensions } from '~/hooks/useRefDimensions'
import { createRef, memo, useEffect, useRef, useState } from 'react'
import Typography from '../Typography'
import { useWindowSize } from '~/hooks/useWindowSize'
import { debounce } from '~/lib/utils'
import { spacingString } from '~/styles/theme'

export const LineNumberContainer = memo(
    ({ children }: { children?: React.ReactNode }) => {
        const [totalLine, setTotalLine] = useState(0)
        const ref = useRef<HTMLDivElement>(null)

        useEffect(() => {
            if (!ref.current) return
            const resizeObserver = new ResizeObserver(() => {
                console.log('resize', ref.current?.getBoundingClientRect())
                if (ref.current) {
                    const { height: refHeight } = ref.current.getBoundingClientRect()
                    setTotalLine(Math.round(refHeight / 25))
                }
            })
            resizeObserver.observe(ref.current)
            return () => resizeObserver.disconnect() // clean up
        }, [ref])

        return (
            <Div
                $height="100%"
                $padding={{
                    y: 8,
                }}
                ref={ref}
            >
                <StyledCodeDiv
                    $padding={{
                        x: 3,
                        y: 8,
                    }}
                    $hiddenAs="mobile"
                >
                    {Array.from({ length: totalLine }).map((_, index) => (
                        <Typography key={index} color="gray" variant="subtitle" component="div" align="right">
                            {index + 1}
                        </Typography>
                    ))}
                </StyledCodeDiv>
                <StyledContainer>{children}</StyledContainer>
            </Div>
        )
    },
    () => true,
)

const StyledContainer = styled(Container)`
    margin-bottom: 4rem;
    @media (max-width: 1400px) {
        margin-left: 60px;
        width: calc(100% - 75px);
    }
    @media (max-width: ${({ theme }) => theme.breakpoints.mobile}) {
        margin-left: 0;
        width: 100%;
        padding: 0 ${({ theme }) => spacingString(5, theme)};
    }
`

const StyledCodeDiv = styled(Div)`
    pointer-events: none;
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0.5;
    user-select: none;
    height: 100%;
    width: 55px;
`

LineNumberContainer.displayName = 'LineNumberContainer'